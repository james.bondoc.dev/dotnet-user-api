﻿using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Threading.Tasks;

namespace User.Api.Controllers
{
    [Route("users")]
    [ApiController]
    public class UserController : ControllerBase
    {
        public HttpClient Http { get; set; }

        public UserController()
                => Http = new HttpClient();


        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetUsers()
        {
            var response = await Http.GetAsync("https://randomuser.me/api/?results=1000");
            var responseBody = await response.Content.ReadAsStringAsync();

            return Ok(responseBody);
        }


    }
}
